node {
    def showtimes
    def movies
    def users
    def bookings

    stage('Preparation') {
      echo 'Cloning/fetching developed branch from project maintained on GitLab'
      git branch: 'przemek.ci', url: 'https://gitlab.com/itacademywroclaw/microservices'
      echo 'Clone completed!'
    }
    stage('Build') {
       echo 'Building Docker images'
       sh 'ls'
       showtimes = docker.build("splendid-flow-230714/showtimes-app", "./showtimes")
       movies = docker.build("splendid-flow-230714/movies-app", "./movies")
       users = docker.build("splendid-flow-230714/users-app", "./users")
       bookings = docker.build("splendid-flow-230714/bookings-app", "./bookings")
       sh 'docker images'
       echo 'Builds completed!'
    }
    stage('Integration Tests') {
       echo 'Running tests that check communication between services'
       sh 'docker images'
       sh 'docker-compose up -d'
       sh 'docker ps'
       sh 'python users/tests/users.py'
       sh 'python movies/tests/movies.py'
       sh 'python showtimes/tests/showtimes.py'
       sh 'python bookings/tests/bookings.py'
       echo 'Tests completed!'
    }
    stage('Push to cloud') {
       echo 'Pushing to the Google Container Registry'
       docker.withRegistry('https://gcr.io', 'gcr:splendid-flow-230714') {
           users.push("${env.BUILD_NUMBER}")
           showtimes.push("${env.BUILD_NUMBER}")
           movies.push("${env.BUILD_NUMBER}")
           bookings.push("${env.BUILD_NUMBER}")   
        }
       echo 'Push completed!' 

    }
    stage('Clean-up the test environment') {
       echo 'Removing remaining containers'
       sh 'docker stop $(docker ps -aq)'
       sh 'docker rm $(docker ps -aq)'
       echo 'Removing remaining images'
       sh 'docker images -q'
       sh 'docker rmi -f $(docker images -q)'
       echo 'Removing remaining networks'
       sh 'docker network ls'
       sh 'docker network prune -f'
       echo'Clean-up completed!'
    }

}